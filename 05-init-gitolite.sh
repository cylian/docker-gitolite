#!/bin/sh

### SSH_KEY - This variable IS mandatory
if test -z "${SSH_KEY-}"
then
	echo ''
	echo 'You need to specify SSH_KEY on first run to setup gitolite'
	echo 'e.g: docker run -e SSH_KEY="$(cat ~/.ssh/id_rsa.pub)" -e SSH_KEY_NAME="$(whoami)" gitolite'
	echo ''
	exit 0
fi

### SSH_KEY_FILE - This variable is inferred
SSH_KEY_FILE=/tmp/${SSH_KEY_NAME:=admin}.pub

### Setup gitolite
echo "${SSH_KEY}" > ${SSH_KEY_FILE}
gitolite setup -pk ${SSH_KEY_FILE}

### Cleanup
rm ${SSH_KEY_FILE}

### Completed.
rm -f ${0}
