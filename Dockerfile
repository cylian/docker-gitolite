FROM alpine:3.11

LABEL maintener='Cylian <contact+docker@cylian.eu>'

COPY ./entrypoint.sh /entrypoint.sh
COPY ./*-*.sh /entrypoint.d/

RUN	set -x ;\
	addgroup -S git ;\
	adduser -h /git -g git -s /bin/sh -G git -S git ;\
	passwd -u git ;\
	chown -R git:git /entrypoint.* ;\
	chmod 500 /entrypoint.sh /entrypoint.d/*.sh ;\
	apk --no-cache add gitolite openssh-server ;\
	test -d /var/lib/git && rmdir /var/lib/git ;\
	true

### Install Openssh - Generate host keys - but they will wtock to the docker image
#RUN	set -x ;\
#	apk --no-cache add openssh-server ;\
#	ssh-keygen -q -N '' -f /etc/ssh/ssh_host_rsa_key -t rsa ;\
#	ssh-keygen -q -N '' -f /etc/ssh/ssh_host_ecdsa_key -t ecdsa ;\
#	ssh-keygen -q -N '' -f /etc/ssh/ssh_host_ed25519_key -t ed25519 ;\
#	chown -R git:git /etc/ssh ;\
#	true

### Patch openssh config - SED is too painful - use a separate file instead
#RUN	sed -i /etc/ssh/sshd_config \
#		 -e 's|^[#]*Port[[:space:]].*$|Port 2200|' \
#		 -e 's|^[#]*PubkeyAuthentication[[:space:]].*$|PubkeyAuthentication yes|' \
#		 -e 's|^[#]*PasswordAuthentication[[:space:]].*$|PasswordAuthentication no|' \
#		 -e 's|^[#]*PermitRootLogin[[:space:]].*$|PermitRootLogin no|' \
#		 -e 's|^[#]*HostKey[[:space:]]/etc/ssh/\(.*\)$|HostKey /etc/ssh/keys/\1|' \
#		 -e 's|^[#]*Subsystem[[:space:]].*$|#&|'

COPY sshd.cfg /etc/ssh/sshd_config

EXPOSE 2200

WORKDIR /git
USER git

ENTRYPOINT ["/bin/sh","/entrypoint.sh"]
CMD ["/usr/sbin/sshd","-D"]
