#!/bin/sh

_uid=$(id -un)
_gid=$(id -gn)

### Display current user
echo Running as ${_uid}:${_gid}

### Correct volumes
test -O ${HOME} || echo [init] Warning: uid != ${_uid} for ${HOME}
test -G ${HOME} || echo [init] Warning: gid != ${_gid} for ${HOME}

