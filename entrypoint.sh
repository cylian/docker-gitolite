#!/bin/sh
set -e
set -u

### Debug mode
test -z "${DEBUG:=}" || DEBUG='-x'

### Export required variables
test -n "${USER-}" || export USER=$(id -un)

### Run entrypoint scripts - Failure not allowed
find /entrypoint.d -name '*.sh' -type f -executable -print | sort | while read _cmd
do
	/bin/sh ${DEBUG} ${_cmd}
done

### Start command
test ${#} -gt 0 && command "${@}"
