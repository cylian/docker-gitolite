#!/bin/sh

### Variables
_key_base=/git/keys

### Check key directory
test -e ${_key_base} || mkdir ${_key_base}
test -O ${_key_base} || chown $(id -u) ${_key_base}
test -G ${_key_base} || chgrp $(id -g) ${_key_base}
chmod 700 ${_key_base}

### Set host key
for _key_algorithm in rsa ecdsa ed25519
do

	_key_file=${_key_base}/ssh_host_${_key_algorithm}_key

	if test ! -e ${_key_file}
	then
		echo "[ssh] Creating ${_key_file}"
		ssh-keygen -q -N '' -f ${_key_file} -t ${_key_algorithm}
	fi
done
